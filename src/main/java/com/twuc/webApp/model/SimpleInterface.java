package com.twuc.webApp.model;

import org.springframework.stereotype.Component;
@Component
public interface SimpleInterface {

    public String getName();
}
