package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleDependent {

    private String name;

    public SimpleDependent() {
    }

    @Bean
    public String getName() {
        setName();
        return name;
    }

    public void setName() {
        this.name = "O_o";
    }

}
