package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SimpleObject implements SimpleInterface {

    @Autowired
    private String name;

    public SimpleObject() {
    }


    public SimpleObject(SimpleDependent simpleDependent) {
        this.name = simpleDependent.getName();

    }

    @Override
    public String getName() {
        return this.name;
    }
}
