package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;

    public Logger getLogger() {
        return logger;
    }

    private Logger logger;

    public WithAutowiredMethod(Dependent dependent, Logger logger) {
        this.dependent = dependent;
        this.logger = logger;
        logger.mark("this is construction method");

    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
        logger.mark("this is initialize method");
    }
}
