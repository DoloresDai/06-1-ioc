package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Logger {

    private List<String> list = new ArrayList<>();

    public void mark(String log) {
        this.list.add(log);
    }

    public List<String> getList() {
        return list;
    }
}
