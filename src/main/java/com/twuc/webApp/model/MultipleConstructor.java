package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private String name;
    private Dependent dependent;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String name) {
        this.name = name;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
