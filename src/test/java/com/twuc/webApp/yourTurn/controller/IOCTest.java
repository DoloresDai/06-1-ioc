package com.twuc.webApp.yourTurn.controller;

import com.twuc.webApp.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class IOCTest {
    @Test
    void should_create_WithoutDependency_with_AnnotationConfigApplicationContext() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);

        assertNotNull(withoutDependency);
    }

    @Test
    void should_create_withDependency_with_AnnotationConfigApplicationContext() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        WithDependency bean = context.getBean(WithDependency.class);

        assertNotNull(bean);
        assertNotNull(bean.getDependent());
    }

    @Test
    void should_throw_error_when_out_of_scanning_scope() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");

        assertThrows(RuntimeException.class, () -> context.getBean(OutOfScanningScope.class));
    }

    @Test
    void should_be_same_when_get_different_bean() {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        WithoutDependency bean1 = context.getBean(WithoutDependency.class);
        WithoutDependency bean2 = context.getBean(WithoutDependency.class);
        WithoutDependency bean3 = new WithoutDependency();

        assertNotEquals(bean1, bean2);
        assertNotEquals(bean1, bean3);
    }

    @Test
    void should_get_name_O_o_when_getBean_SimpleInterface_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        SimpleInterface bean = context.getBean(SimpleInterface.class);

        assertEquals("O_o", bean.getName());
    }

    @Test
    void should_use_first_constructor_when_use_autowired_annotation() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);

        assertNotNull(bean);
        assertEquals(MultipleConstructor.class, bean.getClass());
        assertEquals(Dependent.class, bean.getDependent().getClass());
    }

    @Test
    void should_call_both_constructor_and_initialize() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);

        assertNotNull(bean);
        assertEquals("this is construction method",bean.getLogger().getList().get(0));
        assertEquals("this is initialize method",bean.getLogger().getList().get(1));
    }

    @Test
    void should_create_array_about_three_impl() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.model");
        InterfaceWithMultipleImpls bean = context.getBean(InterfaceWithMultipleImpls.class, ImplementationA.class, ImplementationB.class, ImplementationC.class);

        assertEquals(ImplementationA.class,bean.getClass());
        //TODO
    }
}

